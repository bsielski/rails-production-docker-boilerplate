#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

. ./envs/.env-db

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/1"
echo "Dump DB..."
docker-compose exec db pg_dump -U ${POSTGRES_USER} -Fc "${POSTGRES_PRODUCTION}" -f /tmp/db-backups/"${DUMP_NAME}"-$(date +"%F-T-%H%M%S").manual.dump
