#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/1"
echo "Setup DB..."
docker-compose exec app bundle exec rails db:setup

docker ps -a
