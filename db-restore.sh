#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

DB_TO_RESTORE=$(basename "$1")

. ./envs/.env-db

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/2"
echo "Stop app..."
docker-compose stop app

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 2/2"
echo "Drop DB..."
docker-compose exec db dropdb -U ${POSTGRES_USER} --if-exists $POSTGRES_PRODUCTION


echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 2/2"
echo "Create DB..."
docker-compose exec db createdb -U ${POSTGRES_USER} $POSTGRES_PRODUCTION

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 2/2"
echo "Restore DB..."
docker-compose exec db pg_restore -U ${POSTGRES_USER} -d $POSTGRES_PRODUCTION /tmp/db-backups/"${DB_TO_RESTORE}"

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/2"
echo "Start app..."
docker-compose start app

docker ps -a
