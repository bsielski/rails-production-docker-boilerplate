#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

APP_DIR=app-context/app-src

. ./envs/.env-repo

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/4"
if [[ ! -d "./$APP_DIR" ]]
then
    echo "'$APP_DIR' directory doesn't exist."
    mkdir $APP_DIR
    echo "'$APP_DIR' directory created."
    git clone $REPO_LINK $APP_DIR
    echo "Repository cloned to $APP_DIR directory."
else
    echo "'$APP_DIR' directory exists."
    git -C $APP_DIR pull
    echo "Repository pulled."
fi

./up.sh
