#!/bin/bash
set -o errexit
. env-constants

if [[ ! -d "./envs" ]]
then
    echo "'envs' directory doesn't exist."
    mkdir envs
    echo "'envs' directory created."
fi

printf "REPO_LINK=${REPO_LINK}\n" > envs/.env-repo

printf "POSTGRES_USER=${POSTGRES_USER}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
POSTGRES_HOST=${POSTGRES_HOST}
POSTGRES_DEVELOPMENT=${POSTGRES_DEVELOPMENT}
POSTGRES_TEST=${POSTGRES_TEST}
POSTGRES_PRODUCTION=${POSTGRES_PRODUCTION}
DUMP_NAME=${DUMP_NAME}\n"> envs/.env-db

printf "COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME}
IS_API=${IS_API}\n" > .env

printf "POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
SECRET_KEY_BASE=${SECRET_KEY_BASE}\n"> envs/.env-app

printf "VIRTUAL_HOST=${VIRTUAL_HOST}\n"> envs/.env-rev-proxy
