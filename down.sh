#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/1"
echo "Dump DB..."
./db-dump.sh

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/1"
echo "Down services..."

docker-compose down

docker ps -a
