#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

echo "-------------------------------------"
echo "Script ${SCRIPT_NAME}', step 1/2"
echo "Create DB..."
docker-compose exec app bundle exec rails db:create

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 2/2"
echo "Migrate DB..."
docker-compose exec app bundle exec rails db:migrate

docker ps -a
