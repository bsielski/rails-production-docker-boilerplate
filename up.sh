#!/bin/bash
set -o errexit
SCRIPT_NAME=$(basename "$0")

DUMP_DIR=db-dumps

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 2/4"
echo "Build default docker image..."
docker-compose build app

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 1/4"
if [[ ! -d "./$DUMP_DIR" ]]
then
    echo "'$DUMP_DIR' directory doesn't exist."
    mkdir $DUMP_DIR
    echo "'$DUMP_DIR' directory created."
else
    echo "'$DUMP_DIR' directory exists."
fi

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 3/4"
echo "Start services..."
docker-compose up -d

echo "-------------------------------------"
echo "Script '${SCRIPT_NAME}', step 4/4"
echo "Delete temporary stuff if you want to save a disk space..."
docker system prune -a

docker ps -a
